import java.util.Comparator;

public class CustomDigitComparator implements Comparator<Integer> {

    @Override
    public int compare(Integer o1, Integer o2) {
        if (o1 == null || o2 == null) {
            throw new CustomDigitComparatorException("The values must not be null!!");
        }
        if (o1 % 2 < o2 % 2) {
            return -1;
        }
        if (o1 % 2 > o2 % 2) {
            return 1;
        }
        if (o1 % 2 == o2 % 2) {
            return o1.compareTo(o2);
        }
        return 0;
    }
}

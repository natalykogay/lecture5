public class CustomDigitComparatorException extends RuntimeException {
    /**
     * Конструктор {@code CustomDigitComparatorException без сообщения об ошибке.
     */
    public CustomDigitComparatorException() {
        super();
    }

    /**
     * Конструктор {@code CustomDigitComparatorException с детальным сообщением об ошибке.
     *
     * @param message сообщение при получении ошибки.
     */
    public CustomDigitComparatorException(String message) {
        super(message);
    }

}

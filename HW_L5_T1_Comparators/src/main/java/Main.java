import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<Integer> list = Arrays.asList(4, 5, 1, 2, 34, 60, 82, 9, 7, 208, 3, 11, 2, 50, 66, 8, 9, 71, 100);
        CustomDigitComparator comparator = new CustomDigitComparator();
        list.sort(comparator);
        System.out.println(list);
    }
}

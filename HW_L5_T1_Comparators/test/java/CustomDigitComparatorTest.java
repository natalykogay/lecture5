import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class CustomDigitComparatorTest {
    CustomDigitComparator comparator = new CustomDigitComparator();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();


    /**
     * Успешное выполнение метода compare.
     * Проверяем, что сначала отбирается четное число.
     */
    @Test
    public void comparator1Test() {
        Assert.assertEquals(1, comparator.compare(1, 2));
    }

    /**
     * Успешное выполнение метода compare
     * Проверяем, что сначала отбирается четное число.
     */
    @Test
    public void comparatorTest() {
        Assert.assertEquals(-1, comparator.compare(2, 1));
    }


    /**
     * Успешное выполнение метода compare
     * Проверяем, что сначала отбирается четное число с меньшим значением.
     */
    @Test
    public void comparatorEvenTest() {
        Assert.assertEquals(-1, comparator.compare(2, 6));
    }

    /**
     * Успешное выполнение метода compare
     * Проверяем, что сначала отбирается нечетное число с меньшим значением.
     */
    @Test
    public void comparatorOddTest() {
        Assert.assertEquals(-1, comparator.compare(5, 7));
    }

    /**
     * Неуспешное выполнение метода compare
     * Если передаваемый параметр is null, то падает исключение.
     */
    @Test
    public void comparatorNullTest() {
        expectedException.expect(CustomDigitComparatorException.class);
        expectedException.expectMessage("The values must not be null!!");
        Assert.assertEquals(0, comparator.compare(null, 2));
    }
}
